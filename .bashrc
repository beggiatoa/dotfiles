#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

## Aliases
alias ls='ls --color=auto'

## Prompt
PS1='[\u@\h \W]\$ '

## pfetch
pfetch

## Added by beggiatoa/dotfiles
alias dof="/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME"
