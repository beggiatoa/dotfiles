# dotfiles

_Operative system-level configuration files, or "dotfiles"._

Configuration files for different machines are stored in separate branches.  Each branch is named after the machine host name.

## Machine list

- **beggiatoa**: My laptop PC configured with Arch Linux (linux-lts) and orbiting around [GNU Emacs](https://www.gnu.org/software/emacs/)
- **gclab**: My workspace server configured with Ubuntu 18.04.  Currently serviced by me for all members of the G.Collins' Laboratory.

# beggiatoa configuration

Dotfiles are maintained with a bare git repository as suggested by [Nicola Paolucci](https://www.atlassian.com/git/tutorials/dotfiles)

## Requirements

- Git
